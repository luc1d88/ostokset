'use strict'

const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('./socket/socket-io')(http);
const env = require('dotenv').config();
const port = process.env.PORT || 8080;
const passportJwt = require('./config/passport-jwt.js').configureJwtStrategy();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const db = require ('./config/mongoose');
const path = require('path');
const cors = require('cors');

const auth = require('./routes/auth-routes');
const category = require('./routes/category-routes');
const item = require('./routes/item-routes');
const list = require('./routes/list-routes');
const user = require('./routes/user-routes');
const recipe = require('./routes/recipe-routes');
const recipeItem = require('./routes/recipeItem-routes');

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '/dist')));
app.use(cors());
app.use(function(req,res,next){
    req.io = io;
    next();
});

app.use('/auth', auth);
app.use('/api', category);
app.use('/api', item);
app.use('/api', list);
app.use('/api', recipe);
app.use('/api', recipeItem);
app.use('/api', user);

app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

http.listen(port, function() {
    console.log("\x1b[32mRUNNING: \x1b[0mBackend is up and running on PORT: " + port);
})

module.exports = app; // for testing
