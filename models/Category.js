'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const categorySchema = new Schema({
    name: { type: String, required: true },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'Owner' },
    restricted: { type: Boolean, required: true, default: true }
});

categorySchema.pre('remove', function(next) {

});

module.exports = mongoose.model('Category', categorySchema);
