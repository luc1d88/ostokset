'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema
const randomstring = require("randomstring");

const listSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: false },
    friendCode: { type: String, required: true },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' }
});

listSchema.pre('remove', function(next) {

});

listSchema.methods.generateFriendCode = function(){
  return randomstring.generate({
    length: 5,
    charset: 'alphabetic'
  });
};

module.exports = mongoose.model('List', listSchema);
