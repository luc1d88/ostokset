'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const listItemSchema = new Schema({
    list: { type: Schema.Types.ObjectId, required: true, ref: 'List' },
    item: { type: Schema.Types.ObjectId, required: true, ref: 'Item' },
    checked: { type: Boolean, required: true, default: false},
    quantity: { type: Number, required: true, default: 1 },
    priority: { type: String, enum : ['low','medium','high'], required: true, default: 'medium' },
});

listItemSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('ListItem', listItemSchema);
