'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const recipeSchema = new Schema({
    name: { type: String, required: true },
    category: { type: String, required: true },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    instructions: { type: String }
});

recipeSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('Recipe', recipeSchema);
