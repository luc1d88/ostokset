'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const itemSchema = new Schema({
    name: { type: String, required: true },
    category: { type: Schema.Types.ObjectId, required: true, ref: 'Category' },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    restricted: { type: Boolean, required: true, default: true }
});

itemSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('Item', itemSchema);
