'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const recipeItemSchema = new Schema({
    recipe: { type: Schema.Types.ObjectId, required: true, ref: 'Recipe' },
    item: { type: Schema.Types.ObjectId, required: true, ref: 'Item' },
    group: { type: String, required: false },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    quantity: { type: String, required: true }
});

recipeItemSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('RecipeItem', recipeItemSchema);
