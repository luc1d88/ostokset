'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const userSchema = new Schema({
    displayName: { type: String, required: true },
    profilePicture: { type: String, required: true },
    email: { type: String, required: true },
    approvedTermsOfUse: { type: Boolean, required: true, default: false }
});

userSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('User', userSchema);
