'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const listShareSchema = new Schema({
    list: { type: Schema.Types.ObjectId, required: true, ref: 'List' },
    user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    blocked: { type: Boolean, required: true, default: false }
});

listShareSchema.pre('remove', function(next) {

});

module.exports = mongoose.model('ListShare', listShareSchema);
