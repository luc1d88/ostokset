let mongoose = require('mongoose');
let Recipe = require('../models/Recipe');
let Category = require('../models/Category');
let User = require('../models/User');
let Item = require('../models/Item');
let RecipeItem = require('../models/RecipeItem');
let List = require('../models/List');
let ListItem = require('../models/ListItem');


//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Users', () => {
  let user;
  let recipe;
  let recipeItem;
  let list;
  let listItem;
  let category;
  let item;

  beforeEach('Create user for testing purposes', (done) => { 
    const user = new User({
      _id: '5ad41f71556f14001455f7d0',
      email: 'osq195@gmail.com',
      displayName: 'Oskari Forsblom',
      profilePicture:  'something'
    });
    
    user.save((err, user) => { 
      this.user = user;
      done();         
    }); 
  });

  beforeEach('Create a new recipe', (done) => {
    const newRecipe =  new Recipe({
        name: 'Testi',
        category: 'Testi',
        owner: this.user._id,
        items: [],
        instructions: [],
      });
    newRecipe.save((err, recipe) => {
        this.recipe = recipe;
        done();
    });
  });

  beforeEach('Create a new category', (done) => { 
    const newCategory = new Category({
      name: 'Testi',
      restricted: false,
      owner: this.user._id,
    });
    newCategory.save((err, category) => {
      this.category = category;
      done();         
    });
  });

  beforeEach('Create a new item', (done) => {
    const newItem = new Item({
      name: 'Testi',
      restricted: false,
      category: this.category._id,
      owner: this.user._id,
    });
    newItem.save((err, item) => {
      this.item = item;
      done();
    });
  });

  beforeEach('Create a new recipe item', (done) => {
    const newRecipeItem = new RecipeItem({
      item: this.item,
      owner: this.user._id,
      recipe: this.recipe,
      group: '',
      quantity: '1 dl'
    });
    newRecipeItem.save((err, recipeItem) => {
      this.recipeItem = recipeItem;
      done();
    });
  }); 
  beforeEach('Create a new list', (done) => {
    const newList = new List({
      owner: this.user._id,
      name: 'Testi',
      restricted: false
    });
    newList.save((err, list) => {
      this.list = list;
      done();
    });
  }); 
  beforeEach('Create a new list item', (done) => {
    const newListItem = new ListItem({
      item: this.item,
      owner: this.user._id,
      list: this.list,
      quantity: 3
    });
    newListItem.save((err, listItem) => {
      this.recipeItem = listItem;
      done();
    });
  }); 
  describe('/DELETE User data', (done) => {
    it('it should remove all the user data', (done) => {
      chai.request(server)
        .delete('/api/users/' + this.user._id)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Käyttäjätiedot poistettu onnistuneesti');
        done();
      });
    });
  });
  describe('/GET User terms of use approvement', (done) => {
    it('it should get terms of use approvement', (done) => {
      chai.request(server)
        .get('/api/users/' + this.user._id + '/terms')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('approvedTermsOfUse').eql(false);
        done();
      });
    });
  });
  describe('/PUT User terms of use approvement', (done) => {
    it('it should update terms of use approvement', (done) => {
      chai.request(server)
        .put('/api/users/' + this.user._id + '/terms')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .send( {approvedTermsOfUse: true })
        .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('approvedTermsOfUse').eql(true);
        done();
      });
    });
  });
  afterEach('Remove recipes', (done) => {
    Recipe.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove items', (done) => {
    Item.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove categories', (done) => {
    Category.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove recipe items', (done) => {
    RecipeItem.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove list', (done) => {
    List.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove list items', (done) => {
    ListItem.remove({}, (err) => {
      done();
    });
  });

  afterEach('Remove users', (done) => { 
    User.remove({}, (err) => {
      done();
    });
  });
});
