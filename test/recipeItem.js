let mongoose = require('mongoose');
let Recipe = require('../models/Recipe');
let Category = require('../models/Category');
let User = require('../models/User');
let Item = require('../models/Item');
let RecipeItem = require('../models/RecipeItem');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

//Our parent block
describe('RecipeItems', () => {
    let recipe; 
    let item;
    let category;
    let recipeItem;

    before('Remove recipes', (done) => {
        Recipe.remove({}, (err) => {
            done();
        })
    });

    before('Remove items', (done) => {
        Item.remove({}, (err) => {
            done();
        })
    });

    before('Remove categories', (done) => {
        Category.remove({}, (err) => {
            done();
        })        
    });

    before('Create user for testing purposes', (done) => { 
        const user = new User({
          _id: '5ad41f71556f14001455f7d0',
          email: 'osq195@gmail.com',
          displayName: 'Oskari Forsblom',
          profilePicture:  'something',
          approvedTermsOfUse: true
        });
        
        user.save((err, user) => { 
            done();         
        });     
    });

    beforeEach('Create a new recipe', (done) => {
        const newRecipe =  new Recipe({
            name: 'Testi',
            category: 'Testi',
            owner: '5ad41f71556f14001455f7d0',
            items: [],
            instructions: [],
          });
        newRecipe.save((err, recipe) => {
            this.recipe = recipe;
            done();
        });
    });

    beforeEach('Create a new category', (done) => { 
        const newCategory = new Category({
          name: 'Testi',
          restricted: false,
          owner: '5ad41f71556f14001455f7d0'
        });
        newCategory.save((err, category) => {
            this.category = category;
            done();         
        });
    });

    beforeEach('Create a new item', (done) => {
        const newItem = new Item({
            name: 'Testi',
            restricted: false,
            category: this.category._id,
            owner: '5ad41f71556f14001455f7d0'
        });
        newItem.save((err, item) => {
            this.item = item;
            done();
        });
    });

    beforeEach('Create a new recipe item',(done) => {
        const newRecipeItem = new RecipeItem({
            item: this.item,
            owner: '5ad41f71556f14001455f7d0',
            recipe: this.recipe,
            group: '',
            quantity: '1 dl'
        });
        newRecipeItem.save((err, recipeItem) => {
            this.recipeItem = recipeItem;
            done();
        });
    });

    describe('/POST RecipeItem', (done) => {
        it('it should POST a new recipe item', (done) => {
            const recipeItem = {
                item: this.item._id,
                recipe: this.recipe._id,
                quantity: '1 dl',
                group: ''
            }

            chai.request(server)
                .post('/api/recipes/' + this.recipe._id + '/items')
                .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
                .send(recipeItem)
                .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id')
                res.body.should.have.property('item')
                res.body.should.have.property('recipe')
                res.body.should.have.property('quantity')
                res.body.should.have.property('group')
                done();
            });
        });
    });

    describe('/DELETE recipe item', (done) => {
        it('it should DELETE the recipe item', (done) => { 
            chai.request(server)
            .delete('/api/recipes/' + this.recipe._id + '/items/' + this.recipeItem._id)
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id').eql(this.recipeItem._id.toString());

                done();
            });
        })
    });

    describe('/PUT recipe item', (done) => {
        it('it should PUT the updated recipe item', (done) => { 
            this.recipeItem.quantity = '3 dl';
            chai.request(server)
            .put('/api/recipes/' + this.recipe._id + '/items/' + this.recipeItem._id)
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
            .send(this.recipeItem)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('quantity').eql(this.recipeItem.quantity);
                done();
            });
        })
    });

    describe('/GET recipe with recipe item', (done) => {
        it('it should GET a specific recipe with recipe items', (done) => {
            chai.request(server)
            .get('/api/recipes/' + this.recipe._id)
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
            .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('items');
            done();
            });
        }); 
    });

    afterEach('Remove recipes', (done) => {
        Recipe.remove({}, (err) => {
            done();
        });
    });

    afterEach('Remove items', (done) => {
        Item.remove({}, (err) => {
            done();
        });
    });

    afterEach('Remove categories', (done) => {
        Category.remove({}, (err) => {
            done();
        });
    });

    afterEach('Remove recipe items', (done) => {
        RecipeItem.remove({}, (err) => {
            done();
        });
    });

    after('Remove users', (done) => { 
        User.remove({}, (err) => {
          done();
        })
    });
});
