//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let Item = require('../models/Item');
let Category = require('../models/Category');
let User = require('../models/User');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Items', () => {
  before((done) => { //Before each test we empty the database
    Item.remove({}, (err) => { 
      done()
    });
  });  
  before((done) => { 
    const user = new User({
      _id: '5ad41f71556f14001455f7d0',
      email: 'osq195@gmail.com',
      displayName: 'Oskari Forsblom',
      profilePicture:  'something',
      approvedTermsOfUse: true
    });
    
    user.save((err) => { 
       done();         
    });     
  });
  /*
  * Test the /GET route
  */
  describe('/GET items', () => {
    it('it should GET all the items', (done) => {
      chai.request(server)
        .get('/api/items')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
          done();
        });
    });
  });

  /*
  * Test the /POST route
  */
  describe('/POST item', () => {
    
    before((done) => { 
      category = new Category({
        _id: '5ad41f71556f1400313213d0',
        name: 'Testi',
        restricted: false,
        owner: '5ad41f71556f14001455f7d0'
      })
      category.save((err) => { 
        done();         
      });
    });
    
    it('it should POST a new item', (done) => {
      const item = {
        name: 'Tuote',
        category: '5ad41f71556f1400313213d0',
        restricted: true
      }

      chai.request(server)
        .post('/api/items')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .send(item)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('it should return a dublication failure message', (done) => {
      const item = {
        name: 'Tuote',
        category: '5ad41f71556f1400313213d0',
        restricted: true
      }

      chai.request(server)
        .post('/api/items')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .send(item)
        .end((err, res) => {
          res.should.have.status(409);      
          done();
        });
    });

    after((done) => { 
      User.remove({}, (err) => {
        done();
      })
    });
  });
  
});
