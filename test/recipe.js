//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let Recipe = require('../models/Recipe');
let User = require('../models/User');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Recipes', () => {
  before('Remove recipes', (done) => { 
    Recipe.remove({}, (err) => { 
       done();         
    });     
  });
  before('Create user for testing purposes', (done) => { 
    const user = new User({
      _id: '5ad41f71556f14001455f7d0',
      email: 'osq195@gmail.com',
      displayName: 'Oskari Forsblom',
      profilePicture:  'something',
      approvedTermsOfUse: true
    });
    
    user.save((err) => { 
       done();         
    });     
  });
  
  /*
  * Test the /GET route
  */
  describe('/GET recipes', () => {
    it('it should GET all the recipes', (done) => {
      chai.request(server)
        .get('/api/recipes')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });

  /*
  * Test the /POST route
  */
  describe('/POST recipe', () => {
    it('it should POST a new recipe', (done) => {
      const recipe =  {
        name: 'Testi',
        category: 'Testi',
        items: [],
        instructions: [],
      }
    
      chai.request(server)
        .post('/api/recipes')
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .send(recipe)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('_id');
          res.body.should.have.property('name');
          res.body.should.have.property('category');
          res.body.should.have.property('instructions');

          done();
        });
    });
  });

  /*
  * Test the /PUT route
  */
  describe('/PUT recipe', () => {
    let recipe

    before((done) => {
      Recipe.findOne({name: 'Testi'}, (err, recipe) => {
        this.recipe = recipe;
        done();
      })
    })

    it('it should PUT a new recipe', (done) => {
      chai.request(server)
        .put('/api/recipes/' + this.recipe._id)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .send(this.recipe)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  /*
  * Test the /GET RECIPE route
  */
  describe('/GET specific recipe', () => {
    let recipe

    before((done) => {
      Recipe.findOne({name: 'Testi'}, (err, recipe) => {
        this.recipe = recipe;
        done();
      })
    })

    it('it should GET a specific recipe', (done) => {
      chai.request(server)
        .get('/api/recipes/' + this.recipe._id)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  /*
  * Test the /DELETE route
  */
  describe('/DELETE recipe', () => {
    let recipe

    before((done) => {
      Recipe.findOne({name: 'Testi'}, (err, recipe) => {
        this.recipe = recipe;
        done();
      })
    })

    it('it should DELETE a new recipe', (done) => {
      chai.request(server)
        .delete('/api/recipes/' + this.recipe._id)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('it should not found the recipe', (done) => {
      chai.request(server)
        .get('/api/recipes/' + this.recipe._id)
        .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQ0MWY3MTU1NmYxNDAwMTQ1NWY3ZDAiLCJlbWFpbCI6Im9zcTE5NUBnbWFpbC5jb20iLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbG9va2FzaWRlLmZhY2Vib29rLmNvbS9wbGF0Zm9ybS9wcm9maWxlcGljLz9hc2lkPTEwMTMyMjAxOTg4NTQ5MzAmaGVpZ2h0PTIwMCZ3aWR0aD0yMDAmZXh0PTE1MjQxMTAzMjAmaGFzaD1BZVRjOG5kalduUWhLSm5rIiwiZGlzcGxheU5hbWUiOiJPc2thcmkgRm9yc2Jsb20iLCJpYXQiOjE1MjM4OTUyNDN9.UJTu0AMbx_WqGtyJcfnyRjCkKNCXRqWicj_7FIOO234')
        .end((err, res) => {
          res.should.have.status(200);
          res.should.not.have.a.property('_id')
          done();
        });
    });
  });


  after((done) => { 
    User.remove({}, (err) => {
      done();
    })
  });
});
