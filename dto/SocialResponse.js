function SocialResponse(email, profilePicture, displayName) {
  this.email = email;
  this.profilePicture = profilePicture;
  this.displayName = displayName;
}

module.exports = SocialResponse;
