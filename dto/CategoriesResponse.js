function CategoriesResponse(success, categories) {
  this.success = success;
  this.categories = categories
}

module.exports = CategoriesResponse;
