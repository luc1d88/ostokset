function TokenResponse(success, token, profile) {
  this.success = success;
  this.token = token;
  this.profile = profile
}

module.exports = TokenResponse;
