'use strict'

const express = require('express');
const Recipe = require('../models/Recipe');
const RecipeItem = require('../models/RecipeItem');
const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = (function() {
  let recipeItems = express.Router();

  /* 
  * PUT a updated item into the recipe
  */ 
  recipeItems.put('/recipes/:recipeId/items/:recipeItemId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const owner = decoded._id;
    const recipeId = req.params.recipeId;
    const recipeItemId = req.params.recipeItemId;
    const recipeItem = req.body;
    const promise = RecipeItem.findOneAndUpdate({owner: owner, _id: recipeItemId}, recipeItem, {new: true}).exec()

    promise.then((recipeItem) => {
      return res.json(recipeItem);
    });

    promise.catch((err) => {
      return res.sendStatus(500);
    });
  });

  /* 
  * DELETE a specific recipe item from the recipe
  */ 
  recipeItems.delete('/recipes/:recipeId/items/:recipeItemId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const owner = decoded._id;
    const recipeId = req.params.recipeId;
    const recipeItemId = req.params.recipeItemId;

    const promise = RecipeItem.findOneAndRemove({owner: owner, _id: recipeItemId}).exec()

    promise.then((recipeItems) => {
      return res.json(recipeItems);
    });

    promise.catch((err) => {
      return res.sendStatus(500);
    });

  });

  /* 
  * POST a new item into the recipe
  */ 
  recipeItems.post('/recipes/:recipeId/items', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const owner = decoded._id;
    const recipeId = req.params.recipeId;
    const item = req.body.item;
    const recipe = req.body.recipe;
    const quantity = req.body.quantity;
    const group = req.body.group;

    const newRecipeItem = new RecipeItem({
      item: item,
      recipe: recipe,
      quantity: quantity,
      group: group,
      owner: owner
    });

    const promise = newRecipeItem.save();

    promise.then((recipeItem) => {
      if (recipeItem) {
        RecipeItem.populate(recipeItem, {path: 'item'}, function(err, recipeItem) {
          return res.json(recipeItem);
        })
      }
    });

    promise.catch((err) => {
      return res.sendStatus(500);
    });
  });

  return recipeItems;
})();