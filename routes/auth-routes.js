'use strict';

const express = require('express');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const passport = require('passport');
const request = require('request');
const env = require('dotenv').config();
const JwtToken = require('../models/JwtToken');
const SocialResponse = require('../dto/SocialResponse');
const ErrorResponse = require('../dto/ErrorResponse');
const TokenResponse = require('../dto/TokenResponse');
const https = require('https');

module.exports = (() => {
    const authentication = express.Router();
    const secret = process.env.JWT_SECRET;

    authentication.post('/verify', (req, res) => {
        const token = req.body.token;
        const secret = process.env.JWT_SECRET;
        jwt.verify(token, secret, function(err, decoded) {
            if(decoded) {
                return res.json({isAuthenticated: true});
            }
            else {
                return res.json({isAuthenticated: false});
            }
        })
    });

    authentication.post('/social/facebook', (req, res) => {
        const token = req.body.token;
        request.get('https://graph.facebook.com/me?access_token=' + token + '&fields=id,name,email,picture.type(large)', (err, r, body) => {
            if (err) { return res.json(new ErrorResponse(false, 'Tokenin tarkistus epäonnistui!')); }
            const fbInfo = JSON.parse(body);
            const socialResponse = new SocialResponse(fbInfo.email, fbInfo.picture.data.url, fbInfo.name)
            handleSocialLogin(res, socialResponse);
        });
    });

    authentication.post('/social/google', (req, res) => {
        const token = req.body.token;
        request.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token, (err, r, body) => {
            if (err) { return res.json(new ErrorResponse(false, 'Tokenin tarkistus epäonnistui!')); }
            const googleInfo = JSON.parse(body);
            const socialResponse = new SocialResponse(googleInfo.email, googleInfo.picture, googleInfo.name);
            handleSocialLogin(res, socialResponse);
        });
    })

    return authentication;

})();

 function handleSocialLogin(res, socialResponse) {
    const secret = process.env.JWT_SECRET;

    User.findOne({ 'email': socialResponse.email }, function (err, user) {
        if (err) { return console.log(err); }
        else if (user) {
            getImage(socialResponse.profilePicture, function(err, imageData){
                if (err) { return res.json(new ErrorResponse(false, 'Käyttäjän tallentaminen epäonnistui!')); }
                user.email = socialResponse.email;
                user.profilePicture = imageData;
                user.displayName = socialResponse.displayName;
                user.save(function(err, user) {
                    if (err) { return res.json(new ErrorResponse(false, 'Käyttäjän tallentaminen epäonnistui!')); }
                    const payload = {_id: user._id, email: user.email, displayName: user.displayName};
                    const token = jwt.sign(payload, secret); 
                    return res.json(new TokenResponse(true, token, user));
                })
            })
        }
        else {                            
            let newUser = new User({ 
                email: socialResponse.email, 
                displayName: socialResponse.displayName,
            });
            getImage(socialResponse.profilePicture, function(err, imageData){
                newUser.profilePicture = imageData;
                newUser.save(function(err, user) {
                    if (err) { return res.json(new ErrorResponse(false, 'Käyttäjän tallentaminen epäonnistui!')); }
                    const payload = {_id: user._id, email: user.email, displayName: user.displayName};
                    const token = jwt.sign(payload, secret);        
                    return res.json(new TokenResponse(true, token, newUser));
                });
            });
        }
    })

    function getImage(url, callback) {
        https.get(url, res => {
            res.setEncoding('base64');
            let body = "data:" + res.headers["content-type"] + ";base64,";
            res.on('data', function (chunk) {
                body += chunk
            });
    
            res.on('end', function () {
                const data = body;
                callback(null, data);
            });
        })
        // Inform the callback of the error.
        .on('error', callback);
    }
};
