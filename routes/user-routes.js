'use strict'

const express = require('express');
const User = require('../models/User');
const Item = require('../models/Item');
const List = require('../models/List');
const ListItem = require('../models/ListItem');
const Recipe = require('../models/Recipe');
const RecipeItem = require('../models/RecipeItem');
const Category = require('../models/Category');
const ListShare = require('../models/ListShare');
const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = (function() {
  let users = express.Router();

  /* 
  * Get users
  */ 
  users.get('/users', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      

    // TODO
  
  });
  
  /* 
  * Get the user
  */ 
  users.get('/users/:userId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const user = req.params.userId;
    
    // TODO

  });

  /* 
  * Update the user's terms of use approvement
  */ 
  users.put('/users/:userId/terms', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const user = decoded._id;
    const termsOfUse = req.body.approvedTermsOfUse;
    User.findOneAndUpdate( {_id: user }, { approvedTermsOfUse: termsOfUse }, { new: true} ).exec()
    .then((user) => {
      return res.json( { approvedTermsOfUse: user.approvedTermsOfUse} );
    })
    .catch((err) => {
      return res.sendStatus(500);
    })

  });

  /* 
  * Get the user's terms of use approvement
  */ 
  users.get('/users/:userId/terms', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const user = req.params.userId;

    User.findById(user).exec()
    .then((user) => {
      return res.json( { approvedTermsOfUse: user.approvedTermsOfUse} );
    })
    .catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * Remove the user
  */ 
  users.delete('/users/:userId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const owner = decoded._id;
    
    User.findOneAndRemove({'_id': owner}).exec()
    .then(function(deletedUser) {
      List.deleteMany({'owner': owner}).exec()
    })
    .then(function(query) {
      Item.deleteMany({'owner': owner}).exec()
    })
    .then(function(query) {
      Category.deleteMany({'owner': owner}).exec()
    })
    .then(function(query) {
      ListShare.deleteMany({'user': owner}).exec()
    })
    .then(function(query) {
      Recipe.deleteMany({'owner': owner}).exec()
    })
    .then(function(query) {
      RecipeItem.deleteMany({'owner': owner}).exec()
    })
    .then(function(query){
      return res.json({message : 'Käyttäjätiedot poistettu onnistuneesti'});
    })
    //Catch all errors and call the error handler;
    .then(null, function(err) {
      console.log('error');
      return res.sendStatus(500);
    });
  });

  return users;
})();