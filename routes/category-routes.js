'use strict'

const express = require('express');
const Category = require('../models/Category');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const CategoriesResponse = require('../dto/CategoriesResponse');
const CategoryResponse = require('../dto/CategoryResponse');
const ErrorResponse = require('../dto/ErrorResponse');

module.exports = (function() {
  const categories = express.Router();

  /* 
  * Get user's categories
  */ 
  categories.get('/categories', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const owner = decoded._id;

    Category.
    find({ }).
    or([{ restricted: false }, {owner: owner}]).
    sort('name').
    select('-owner -restricted').
    exec(function (err, categories) {
      if (err) { console.log(err); return res.sendStatus(500); }
      return res.json(categories);
    });
    
  });

  /* 
  * Create a new category
  */ 
  categories.post('/categories', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const name = req.body.name;
    const owner = decoded._id;
    const restricted = req.body.restricted;

    Category.findOne({}).
    or([
      { $and: [{name: name}, {owner: owner}] },
      { $and: [{name: name}, {restricted: false}] }
    ]).
    exec(function (err, category) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else if (category) { return res.status(409).send('Kategoria on jo olemassa');    }
      else {
        const newCategory = new Category({
          name: name,
          owner: owner,
          restricted: restricted
        });
    
        newCategory.save(function (err, category) {
          if (err) { return res.sendStatus(500) }
          return res.json(category);
        });
      }
    });

  });

  /* 
  * Update the category
  */ 
  categories.put('/categories/:categoryId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);

    // TODO

  });

  /* 
  * Remove the category
  */ 
  categories.delete('/categories/:categoryId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);

    // TODO

  });

  return categories;
})();