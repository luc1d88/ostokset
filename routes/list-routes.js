'use strict'

const express = require('express');
const List = require('../models/List');
const User = require('../models/User');
const ListItem = require('../models/ListItem');
const ListShare = require('../models/ListShare');

const passport = require('passport');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

module.exports = (function() {
  let lists = express.Router();
  /* 
  * Get user's lists
  */ 
  lists.get('/lists', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          

    List.
    find({ }).
    or([{ owner: decoded._id }]).
    sort('name').
    populate('owner').
    exec(function (err, lists) {
      if (err) { console.log(err); return res.sendStatus(500); }
      return res.json(lists);
    });          
  });
  /* 
  * Get users shared lists
  */ 
  lists.get('/lists/shared', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          

    ListShare.
    find({ }).
    or([{ user: decoded._id }]).
    exec(function (err, listShares) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else if (listShares) {
        List.
        find({'_id': { 
          $in: _.map(listShares, 'list')
        }
        }).
        sort('name').
        populate('owner').
        exec(function (err, lists) {
          if (err) { console.log(err); return res.sendStatus(500); }
          return res.json(lists);
        }); 
      }
      else {
        return res.json([]);
      }
    });          
  });
  /* 
  * Get list items
  */ 
  lists.get('/lists/:listId/items', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          
    const list = req.params.listId;

    ListItem.
    find({ }).
    where('list').equals(list).
    sort('name').
    populate({ 
      path: 'item',
      populate: {
        path: 'category',
        model: 'Category'
      }
    }).
    exec(function (err, listItems) {
      if (err) { console.log(err); return res.sendStatus(500); }
      return res.json(listItems);
    });          
  });
  /* 
  * Create a list item
  */ 
  lists.post('/lists/:listId/items', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          

    const name = req.body.name;
    const item = req.body.item;
    const list = req.body.list;
    const quantity = req.body.quantity;
    const priority = req.body.priority;
    const user = decoded._id;

    const newListItem = new ListItem();
    newListItem.name = name;
    newListItem.item = item;
    newListItem.list = list;
    newListItem.quantity = quantity;
    newListItem.priority = priority;

    newListItem.save(function (err, listItem) {
      if (err) { console.log(err); return res.sendStatus(500) }
        ListItem.
        populate(listItem, { path: 'item', populate : { path : 'category'} }, function(err, listItem) {
          if(err) {
            return res.sendStatus(500);
          } else {
            User.findById(user, '_id displayName', function (err, user) {
              if (err) { 
                return res.sendStatus(500); 
              } else {
                req.io.to(list).emit('new-listItem', { user: user, listItem: listItem });
                return res.json(listItem);
              }
            });
          }
      });
    });      
  });
  /*  
  * Update the list item
  */ 
  lists.put('/lists/:listId/items/:itemId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const list = req.params.listId;     
    const item = req.params.itemId;
    const listItem = req.body;
    const user = decoded._id;

    ListItem.
    findByIdAndUpdate(item, listItem, {
      new: true
    }).
    populate({ 
      path: 'item',
      populate: {
        path: 'category',
        model: 'Category'
      }
    }).
    exec(function (err, listItem) {
      if (err) { return res.sendStatus(500); }
      User.findById(user, '_id displayName', function (err, user) {
        if (err) { 
          return res.sendStatus(500); 
        } else {
          req.io.to(list).emit('update-listItem', { user: user, listItem: listItem });
          return res.json(listItem);
        }
      });
    });          
  });
  /*  
  * Clear checked list items
  */
  lists.delete('/lists/:listId/items/',  passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          
    const list = req.params.listId;
    const user = decoded._id;

    ListItem.
    deleteMany({checked: true}).
    populate({ 
      path: 'item',
      populate: {
        path: 'category',
        model: 'Category'
      }
    }).
    exec(function (err) {
      if (err) { console.log(err); return res.sendStatus(500); }
      ListItem.
      find({ }).
      where('list').equals(list).
      sort('name').
      populate({ 
        path: 'item',
        populate: {
          path: 'category',
          model: 'Category'
        }
      }).
      exec(function (err, listItems) {
        if (err) { return res.sendStatus(500); }
        User.findById(user, '_id displayName', function (err, user) {
          if (err) { 
            return res.sendStatus(500); 
          } else {
            req.io.to(list).emit('remove-listItems', { user: user, listItems: listItems });
            return res.json(listItems);
          }
        });
      });          
    });          
  }); 
  /* 
  * Delete a new item
  */
  lists.delete('/lists/:listId/items/:itemId',  passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);          
    const item = req.params.itemId;
    const listItem = req.body;

    ListItem.
    findByIdAndRemove(item).
    populate({ 
      path: 'item',
      populate: {
        path: 'category',
        model: 'Category'
      }
    }).
    exec(function (err, listItem) {
      if (err) { console.log(err); return res.sendStatus(500); }
      return res.json(listItem);
    });      
  }); 
  /* 
  * Create a new list
  */ 
  lists.post('/lists', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    
    const name = req.body.name;
    const owner = decoded._id;
    const description = req.body.description;

    const newList = new List();
    newList.name = name;
    newList.owner = owner;
    newList.friendCode = newList.generateFriendCode();
    newList.description = description;

    newList.save(function (err, list) {
      if (err) { console.log(err); return res.sendStatus(500) }
      List.populate(list, { path: 'owner' }, function(err, list) {
        if (err) { console.log(err); return res.sendStatus(500) }
        return res.json(list);
      });
    });
  });
  /* 
  * Connect to a shared list
  */ 
  lists.post('/lists/:friendCode', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const owner = decoded._id;
    const friendCode = req.params.friendCode;

    List.
    findOne({}).
    where('friendCode').equals(friendCode).
    exec(function (err, list) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else if (!list) { return res.json('list not found')}
      else {
        ListShare.
        findOne({list: list._id, user: owner}).
        exec(function (err, listShare) {
          if (err) { console.log(err); return res.sendStatus(500); }
          else if (listShare) 
            if (listShare.blocked) { return res.json('user is blocked'); }
            else { return res.json('user already added to the list'); }
          else {
            const newListShare = new ListShare();
            newListShare.list = list._id;
            newListShare.user = owner;
            newListShare.save(function (err, shareList) {
              if (err) { console.log(err); return res.sendStatus(500) }
              List.populate(list, { path: 'owner' }, function(err, list) {
                if (err) { console.log(err); return res.sendStatus(500); }
                return res.json(list);
              });
            });
          }
        });
      }
    });
  });

  /* 
  * Get the list
  */ 
  lists.get('/lists/:listId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const list = req.params.listId;
    const owner = decoded._id;

    List.
    findById(list).
    where('owner').equals(owner).
    exec(function (err, _list) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else if (!_list) {
        ListShare.
        findOne({ list: list, user: owner}).
        exec(function (err, listShare) { 
          if (err) { console.log(err); return res.sendStatus(500); }
          else if (listShare) {
            List.findById(listShare.list).
            exec(function (err, __list) {
              if (err) { console.log(err); return res.sendStatus(500); }
              return res.json(__list);
            });
          }
        });
      } else {
        return res.json(_list);
      }
    }); 
  });

  /* 
  * Update the list
  */ 
  lists.put('/lists/:listId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const item = req.body;
    const owner = decoded._id;

    List.findOneAndUpdate({_id: item._id, owner: owner}, item, { new: true}).
    exec(function (err, list) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else if(list) {
        List.populate(list, { path: 'owner' }, function(err, list) {
          if (err) { console.log(err); return res.sendStatus(500); }
          return res.json(list);
        });      
      } else {
      return res.status(409).send('Listaa ei löydy tai sinulla ei ole oikeuksia muokata listaa')}    
    })
  });
  /* 
  * Remove the list
  */ 
  lists.delete('/lists/:listId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const list = req.params.listId;

    List.findByIdAndRemove(list).
    where('owner').equals(decoded._id).
    exec(function (err, _list) {
      if (err) { console.log(err); return res.sendStatus(500); }
      else { 
        ListShare.deleteMany({ list: list}).exec(function(err, listShares) {
          ListItem.deleteMany({ list: list}).exec(function(err, listItems) {
            return res.json(_list); 
          })
        });
      }
    });
  });
  /* 
  * Remove shared list
  */ 
  lists.delete('/lists/shared/:listId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const list = req.params.listId;

    ListShare.
    findOneAndRemove({ list: list, user: decoded._id}).
    exec(function (err, listShare) { 
      if (err) { console.log(err); return res.sendStatus(500); }
      ListShare.
      find({ }).
      or([{ user: decoded._id }]).
      exec(function (err, listShares) {
        if (err) { console.log(err); return res.sendStatus(500); }
        else if (listShares) {
          List.
          find({'_id': { 
            $in: _.map(listShares, 'list')
          }
          }).
          sort('name').
          populate('owner').
          exec(function (err, lists) {
            if (err) { console.log(err); return res.sendStatus(500); }
            return res.json(lists);
          }); 
        }
        else {
          return res.json([]);
        }
      });     
    });
  });

  /* 
  * Get the users in the list
  */ 
 lists.get('/lists/:listId/users', passport.authenticate('jwt', { session: false }), function(req, res) {
  let token = req.header('Authorization');
  token = token.slice(7);
  const decoded = jwt.decode(token);
  const owner = decoded._id;
  const list = req.params.listId;

  ListShare.find({ list: list }).
  exec(function (err, listShares) {
    if (err) { console.log(err); return res.sendStatus(500); }
    else if (listShares) {
      ListShare.populate(listShares, { path: 'user' }, function(err, listShares) {
        if (err) { console.log(err); return res.sendStatus(500); }
        //const users = _.map(listShares, listShare => listShare.user)
        return res.json(listShares);
      });      
    } else {
    return res.status(409).send('Listaa ei löydy tai sinulla ei ole oikeuksia muokata listaa')}    
  })
});

  return lists;
})();