'use strict'

const express = require('express');
const Recipe = require('../models/Recipe');
const RecipeItem = require('../models/RecipeItem');
const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = (function() {
  let recipes = express.Router();

  /* 
  * Get user's recipes
  */ 
  recipes.get('/recipes', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
  
    const promise = Recipe.find({ owner: decoded._id }).sort('category').exec();
    
    promise.then((items) => {
      if (!items) { return res.status(204).send('Reseptejä ei löytynyt')}
      else { return res.json(items); }
    })
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * POST a new recipe
  */ 
  recipes.post('/recipes', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const name = req.body.name;
    const category = req.body.category;
    const owner = decoded._id;
    const instructions = req.body.instructions;
    const items = req.body.items;

    const newRecipe = new Recipe({
      name: name,
      category: category,
      owner: owner,
      items: items,
      instructions: instructions
    });
    
    const promise = newRecipe.save();

    promise.then((newRecipe) => {
      return res.json(newRecipe);
    })
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * PUT a updated recipe
  */ 
  recipes.put('/recipes/:recipeId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const owner = decoded._id;
    const recipeId = req.params.recipeId;
    const recipe = req.body;

    const promise = Recipe.findOneAndUpdate({owner: owner, _id: recipe._id}, recipe, {new: true}).exec();

    promise.then((updatedRecipe) => {
      updatedRecipe = updatedRecipe.toObject();
      updatedRecipe.items = [];
      RecipeItem.find({recipe: recipeId})
      .populate({path: 'item'})
      .exec()
      .then((recipeItems) => {
        updatedRecipe.items = recipeItems
        return res.json(updatedRecipe);
      })
    })
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * GET a specific recipe
  */ 
  recipes.get('/recipes/:recipeId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const owner = decoded._id;
    const recipeId = req.params.recipeId;

    const promise = Recipe.findOne({owner: owner, _id: recipeId}).exec();

    promise.then((recipe) => {
      if (!recipe) { return res.json(recipe); }
      recipe = recipe.toObject();
      recipe.items = [];
      RecipeItem.find({recipe: recipeId})
      .populate({path: 'item'})
      .exec()
      .then((recipeItems) => {
        recipe.items = recipeItems
        return res.json(recipe);
      })
    })
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * DELETE a specific recipe
  */ 
  recipes.delete('/recipes/:recipeId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      
    const owner = decoded._id;
    const recipeId = req.params.recipeId;

    const promise = Recipe.findOneAndRemove({owner: owner, _id: recipeId}).exec();

    promise.then((deletedRecipe) => {
      RecipeItem.deleteMany({ recipe: recipeId }).exec()
      .then(() => {
        return res.json(deletedRecipe);
      });
    });
    promise.catch((err) => {
      return res.sendStatus(500);
    });
  });

  return recipes;
})();