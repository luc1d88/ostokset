'use strict'

const express = require('express');
const Item = require('../models/Item');
const ListItem = require('../models/ListItem');
const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = (function() {
  let items = express.Router();

  /* 
  * Get user's items
  */ 
  items.get('/items', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);      

    const promise = Item.find({ })
    .or([{ restricted: false }, { owner: decoded._id }])
    .sort('name').select('-owner').exec();
    promise.then((items) => {
      if (!items) { return res.status(204).send('Tuotteita ei löytynyt')}
      else { return res.json(items); }
    })
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });
  /* 
  * Create a new item
  */ 
  items.post('/items', passport.authenticate('jwt', { session: false }), (req, res) => {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    
    const name = req.body.name;
    const owner = decoded._id;
    const restricted = req.body.restricted;
    const category = req.body.category;

    const promise = Item.findOne({}).or([
      { $and: [{name: name}, {owner: owner}] },
      { $and: [{name: name}, {restricted: false}] }
    ]).exec()
    promise.then((item) => {
      if (item) {
        res.status(409).send('Tuote on jo lisätty')
      } else {
        const item = new Item({
          name: name,
          owner: owner,
          restricted: restricted,
          category: category
        });
        return item.save()
      }
    })
    .then((newItem) => {
      return res.json(newItem);
    })
    .catch((err) => {
      return res.sendStatus(500);
    })

  });
  
  /* 
  * Update the item
  */ 
  items.put('/items/:itemId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const itemId = req.params.itemId;
    const decoded = jwt.decode(token);
    const name = req.body.name;
    const owner = decoded._id;
    const category = req.body.category;
    
    const promise = Item.findOneAndUpdate({ _id: itemId, owner: owner }, { name: name, category: category }, { new: true })
    .exec()
    promise.then((item) => {
      return res.json(item);
    });
    promise.catch((err) => {
      return res.sendStatus(500);
    })
  });

  /* 
  * Remove the item
  */ 
  items.delete('/items/:itemId', passport.authenticate('jwt', { session: false }), function(req, res) {
    let token = req.header('Authorization');
    token = token.slice(7);
    const decoded = jwt.decode(token);
    const itemId = req.params.itemId;
    const owner = decoded._id;

    const promise = Item.findOneAndRemove({ _id: itemId, owner: owner }).exec()
    promise.then((item) => {
      ListItem.deleteMany({ item: itemId}).exec(function(err, listItems) {
        return res.json(item);
      });
    });
    promise.catch((err) => {
      return res.sendStatus(500);
    });
  });

  return items;
})();