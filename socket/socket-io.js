'use strict'

module.exports = ((http) => {
  const io = require('socket.io')(http);
  io.on('connection', (socket) => {
    console.log('Client connected')
    socket.on('disconnect', () => console.log('Client disconnected'));
    socket.on('join', (data) => { 
      console.log('joining room ' + data.room)
      socket.join(data.room);
    });
    socket.on('leave', (data) => {
      console.log('leaving room ' + data.room) 
      socket.leave(data.room);
    });
  });
  return io;
});

