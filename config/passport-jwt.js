const passport = require('passport');
const User = require('../models/User');

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const env = require('dotenv').config();

const jwtOpts = {};
const secret = process.env.JWT_SECRET

jwtOpts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOpts.secretOrKey = secret;

module.exports.configureJwtStrategy = function () { 
    passport.use(new JwtStrategy(jwtOpts, function(jwt, done) {
        User.findById(jwt._id, function(err, user) {
            if (err) {
                return done(err, false);
            } if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        }); 
    }));
};


