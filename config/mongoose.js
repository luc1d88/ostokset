const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const env = require('dotenv').config();

const url = process.env.MONGO_URL || 'localhost:27017/'
const database = process.env.MONGO_DATABASE || 'ostokset'
const user = process.env.MONGO_USER || ''
const password = process.env.MONGO_PASSWORD || ''

const mongoDB = 'mongodb://' + user + password + url + database
mongoose.connect(mongoDB, {
  
});

const db = mongoose.connection;

db.on('error', () => {
  console.log("Database connection error");
});
db.once('open', () => {
  console.log("Connected to database");
});

exports.db = db;
